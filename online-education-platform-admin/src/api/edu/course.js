import request from '@/utils/request'



export default{

    //添加课程信息
    addCourseInfo(courseInfoVo){
        return request({
            url: '/eduservice/course/add',
            method: 'post',
            data: courseInfoVo
          })
    },

    //查询所有讲师
    getListTeacher(){
        return request({
            url: '/eduservice/teacher/findAll',
            method: 'get'
          })
    },

    //根据课程id查询课程基本信息
    getCourseInfoId(courseId){
        return request({
            url: `/eduservice/course/getCourseInfo/${courseId}`,
            method: 'get'
          })
    },

    //修改课程信息
    updatCourseInfo(courseInfoVo){
        return request({
            url: '/eduservice/course/updateCourse',
            method: 'post',
            data: courseInfoVo
          })
    },
    //根据课程id确认课程信息
    getPublishCourseId(id){
        return request({
            url: `/eduservice/course/getCoursePublishVoById/${id}`,
            method: 'get'
          })
    },
     //课程的最终发布,修改课程的status为Normal即为发布
     publishCourse(id){
        return request({
            url: `/eduservice/course/publishCourse/${id}`,
            method: 'put'
          })
     },
     //课程列表
     getCourseList(){
      return request({
        url: '/eduservice/course/courseList',
        method: 'get'
      })
     },
     //删除课程
     deleteCourseId(id){
      return request({
        url: '/eduservice/course/delete/'+id,
        method: 'delete'
      })
     },

     //分页
     pageCourse(current,limit){
      return request({
          url: `/eduservice/course/${current}/${limit}`,
          method: 'get'
        })
    },

    //条件查询带分页
    getCourseListPage(current,limit,courseQuery){
      return request({
          url: `/eduservice/course/pageCourseCondition/${current}/${limit}`,
          method: 'post',
          data: courseQuery
        })
    }
}
