import request from '@/utils/request'
//导出一个默认的函数
const api_name = '/eduservice/chapter'
// http://localhost:8080/eduserver/teacher/conditions/1/10
export default {
    addChapter(chapterInfo) {
        return request({
            url: `${api_name}/addChapter`,
            method: 'post',
            data: chapterInfo
        })
    },
    //已知课程id 查询这个课程所有的章节和小节
    getChapter(courseId) {
        return request({
            url: `${api_name}/getChapterAll/${courseId}`,
            method: 'get'
        })
    },
    //根据章节id 获取章节信息
    getChapterById(chapterId) {
        return request({
            url: `${api_name}/getChapterInfo/${chapterId}`,
            method: 'get'
        })
    },
    updateChapter(chapter) {
        return request({
            url: `${api_name}/updateChapter`,
            method: 'post',
            data: chapter
        })
    },
    //删除章节
       deleteChapter(chapterId) {
    return request({
            url:  `${api_name}/deleteChapter/${chapterId}`,
            method: 'delete'
          })
    },
    //添加小节
    addVideo(eduVideo){
        return request({
            url: '/eduservice/video/addVideo',
            method: 'post',
            data: eduVideo 
          })
    }, //根据videoId查询小节信息
    getVideoId(videoId){
        return request({
            url: `/eduservice/video/getVideoInfo/${videoId}`,
            method: 'get'
          })
    },
    //修改小节
    updateVideo(eduVideo){
        return request({
            url: '/eduservice/video/update',
            method: 'post',
            data: eduVideo 
          })
    },
    //删除小节
    deleteVideo(videoId){
        return request({
            url: `/eduservice/video/delete/${videoId}`,
            method: 'delete'
          })
    },

    //通过视频id删除阿里云视频
    deleteAliyunVideo(id){
        return request({
            url: `/eduvod/video/delete/${id}`,
            method: 'delete'
          })
    }
}
