import request from '@/utils/request'



export default{

    //添加小节
    addVideo(eduVideo){
        return request({
            url: '/eduservice/video/addVideo',
            method: 'post',
            data: eduVideo 
          })
    },
    //根据videoId查询小节信息
    getVideoId(videoId){
        return request({
            url: `/eduservice/video/getVideoInfo/${videoId}`,
            method: 'get'
          })
    },
    //修改小节
    updateVideo(eduVideo){
        return request({
            url: '/eduservice/video/update',
            method: 'post',
            data: eduVideo 
          })
    },
    //删除小节
    deleteVideo(videoId){
        return request({
            url: `/eduservice/video/delete/${videoId}`,
            method: 'delete'
          })
    },

    //通过视频id删除阿里云视频
    deleteAliyunVideo(id){
        return request({
            url: `/eduvod/video/delete/${id}`,
            method: 'delete'
          })
    }
}
