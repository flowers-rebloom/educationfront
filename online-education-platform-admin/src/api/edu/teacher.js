import request from '@/utils/request'



export default{
    getTeacherListPage(current,limit,teacherQuery){
        return request({
            url: `/eduservice/teacher/pageTeacherCondition/${current}/${limit}`,
            method: 'post',
            data: teacherQuery
          })
    },

    deleteTeacherById(id){
        return request({
            url: `/eduservice/teacher/delete/${id}`,
            method: 'delete'
          })
    },

    save(teacher){
        return request({
            url: '/eduservice/teacher/add',
            method: 'post',
            data: teacher
          })
    },

    //根据id查询讲师信息
    getTeacherInfo(id){
        return request({
            url: `/eduservice/teacher/get/${id}`,
            method: 'get'
          })
    },

    //修改讲师
    updateTeacherInfo(teacher){
        return request({
            url: '/eduservice/teacher/update',
            method: 'post',
            data: teacher
          })
    }


}
