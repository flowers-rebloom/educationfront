import request from '@/utils/request'

export default{
    //统计某一天的注册人数,并添加记录到统计表
    getRegisterCount(day){
        return request({
            url: '/staservice/sta/getRegisterCount/'+day,
            method: 'get'
          })
    },
    //2 获取统计数据
    getDataSta(searchObj) {
        return request({
            url: `/staservice/sta/showData/${searchObj.type}/${searchObj.begin}/${searchObj.end}`,
            method: 'get'
          })
    }
}
